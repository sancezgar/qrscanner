import 'dart:io';

import 'package:flutter/material.dart';
//Modelos
import 'package:qrscanner/src/bloc/scans_bloc.dart';
import 'package:qrscanner/src/models/scan_model.dart';
import 'package:qrscanner/src/utils/utils.dart' as utils;

//Paginas
import 'package:qrscanner/src/pages/direcciones_page.dart';
import 'package:qrscanner/src/pages/mapas_page.dart';

//QR
import 'package:barcode_scan/barcode_scan.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final scansBloc = new ScansBloc();

  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: AppBar(
        title:Text('Qr Scanner'),
        actions: <Widget>[
          IconButton(
            icon:Icon(Icons.delete_forever),
            onPressed: (){
              scansBloc.borrarScansTODOS();
            },
          )
        ],
      ),

      body: _callPage(_currentIndex),

      bottomNavigationBar: _crearBottomNavigationBar(),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.filter_center_focus),
        onPressed: ()=>_scanQR(context),
        backgroundColor: Theme.of(context).primaryColor,
      ),

    );
  }

  _scanQR(BuildContext context) async{
    //https://www.hercon.com.mx
    //geo:21.131741034434512,-101.68187513789066

    String futureString;

    try{
      futureString = await BarcodeScanner.scan();
    }catch(e){
      futureString = e.toString();
    }

    if( futureString != null ) {

      final scan = ScanModel( valor:futureString );
      scansBloc.agregarScan(scan);

      if(Platform.isIOS){
        Future.delayed(Duration(milliseconds: 750 ),(){
          utils.abrirURL(context,scan);
        });
      }else{
        utils.abrirURL( context,scan );
      }
    }

  }

  Widget _callPage(int paginaActual){

    switch(paginaActual){

      case 0:
        return MapasPage();
      case 1:
        return DireccionesPage();

      default:
        return MapasPage();


    }

  }

  Widget _crearBottomNavigationBar(){

    return BottomNavigationBar(
      currentIndex: _currentIndex,
      onTap: (index){
        setState(() {
          _currentIndex = index;
        });
      },
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.map),
          title: Text('mapas')
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.brightness_1),
          title: Text('Direcciones'),
        ),
      ],
    );

  }
}

