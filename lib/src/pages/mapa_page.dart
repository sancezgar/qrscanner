import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:qrscanner/src/models/scan_model.dart';

class MapaPage extends StatefulWidget {

  @override
  _MapaPageState createState() => _MapaPageState();
}

class _MapaPageState extends State<MapaPage> {
  final map = new MapController();

  String tipoMapa = 'streets';

  @override
  Widget build(BuildContext context) {

    final ScanModel scan = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text('Cordenadas QR'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.my_location),
            onPressed: (){

              map.move(scan.getLatLng(), 15);

            },
          ),
        ],
      ),

      body: _crearFlutterMap(scan),
      floatingActionButton: _crearBotonFlotante(context),
    );
  }

  Widget _crearFlutterMap(ScanModel scan){

    return FlutterMap(
      mapController: map,
      options: MapOptions(
        center: scan.getLatLng(),
        zoom:15
      ),
      layers:[
        _crearMapa(),
        _crearMarcadores(scan)
      ]
    );

  }

  _crearMapa(){
    return TileLayerOptions(
      urlTemplate: 'https://api.mapbox.com/v4/'
      '{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}',
      additionalOptions: {
        'accessToken' : 'pk.eyJ1Ijoic2FuY2V6Z2FyIiwiYSI6ImNrOGJ0NHpnaTA5a2czbW4yb2FlbWV4cHYifQ.OtnB1H0VP5t_0HXt4KTz5A',
        'id' : 'mapbox.$tipoMapa'//streets, dark, light, outdoors,satellite 
      }
    );
  }

  _crearMarcadores(ScanModel scan){

    return MarkerLayerOptions(
      markers: <Marker>[
        Marker(
          width: 100.0,
          height: 100.0,
          point: scan.getLatLng(),
          builder: (context) => Container(
            child: Icon(
              Icons.location_on,
              size: 50.0,
              color: Theme.of(context).primaryColor,
            )
          )
        )
      ] 
    );

  }

  Widget _crearBotonFlotante(BuildContext context){

    return FloatingActionButton(
      onPressed: (){

        //streets, dark, light, outdoors,satellite 
        setState(() {
          switch(tipoMapa){
            case 'streets':
              return tipoMapa = 'dark';
            case 'dark':
              return tipoMapa = 'light';
            case 'light':
              return tipoMapa = 'outdoors';
            case 'outdoors':
              return tipoMapa = 'satellite';
            case 'satellite':
              return tipoMapa = 'streets';
            default:
              return tipoMapa = 'streets';
          }
        });


      },
      child: Icon(Icons.repeat),
      backgroundColor: Theme.of(context).primaryColor,
    );

  }
}

